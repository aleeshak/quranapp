package com.example.inshasiddiqui.appsettings;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.preference.ListPreference;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements SharedPreferences.OnSharedPreferenceChangeListener {
    TextView visibletxt, colortxt, sizetxt;
    String valuee = "no change";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        visibletxt = findViewById(R.id.visible_text);
        colortxt = findViewById(R.id.color_text);
        sizetxt = findViewById(R.id.size_text);

        setupSharedPreferences();
        Log.d("tag","value inside oncreate "+ valuee);


    }
    private void setupSharedPreferences() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        sharedPreferences.registerOnSharedPreferenceChangeListener(this);
    }
    // Method to set Visibility of Text.
    private void setTextVisible(boolean display_text) {
        if (display_text == true) {
            visibletxt.setVisibility(View.VISIBLE);
        } else {
            visibletxt.setVisibility(View.INVISIBLE);
        }
    }
    private void setTextshow_e(boolean text) {
        if (text == true) {

            valuee = "eng";
            Log.d("tag","value outside oncreate "+ valuee);
            sizetxt.setText(valuee);
        } else {
            sizetxt.setText("Herobhai");
        }
    }
    private void setTextshow(boolean text) {
        if (text == true) {
            valuee = "urdu";
            Log.d("tag","value outside oncreate "+ valuee);
            sizetxt.setText(valuee);
        } else {
            sizetxt.setText("Herobhai");
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.settings_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

        if (key.equals("urdu_text")) {
            setTextshow(sharedPreferences.getBoolean("urdu_text",true));

        }
        if (key.equals("eng_text")) {
            setTextshow_e(sharedPreferences.getBoolean("eng_text",true));

        }
        ListPreference listPreference = (ListPreference) sharedPreferences;
        CharSequence currText = listPreference.getEntry();
        String currValue = listPreference.getValue();
        Toast.makeText(getApplicationContext(), currValue, Toast.LENGTH_LONG);


    }


    Preference.OnPreferenceChangeListener listener = new Preference.OnPreferenceChangeListener() {
        @Override
        public boolean onPreferenceChange(Preference preference, Object newValue) {
            // newValue is the value you choose
            return true;
        }
    };

listPreference.setOnPreferenceChangeListener(listener);




    @Override
    protected void onDestroy() {
        super.onDestroy();
        android.support.v7.preference.PreferenceManager.getDefaultSharedPreferences(this)
                .unregisterOnSharedPreferenceChangeListener(this);
    }

}
