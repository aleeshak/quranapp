package com.example.inshasiddiqui.quranandroid;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.PreferenceManager;

import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ActionMode;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

public class ParahAyat extends AppCompatActivity implements SharedPreferences.OnSharedPreferenceChangeListener {

    public ArrayList<HashMap<String, String>> ayatList = new ArrayList<>();
    public ArrayList<HashMap<String, Integer>> ayatAudio = new ArrayList<>();
    public int holder;

    public ListAdapter adapter;
    public ListView listView;
    private boolean isActionMode =false;
    String arbi;
    String arbi_trans;

    // getting attached intent data


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_parah);
        setupSharedPreferences();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);

         holder = Integer.parseInt(getIntent().getStringExtra("aid"));
        Log.d("tag", "Value of   : " + holder);


        //setContentView(R.layout.detail_parah);
         listView = (ListView) findViewById(R.id.listView_p);
        DatabaseAccess databaseAccess = DatabaseAccess.getInstance(this);
        DatabaseAccess db = new DatabaseAccess(this);



        db.open();
        ArrayList<HashMap<String, String>> ayatList = new ArrayList<>();
        ayatList = db.getParah_urdu(holder);
        Log.d("tag","size is " + ayatList);
        if(holder < ayatList.size()) {
            HashMap<String, String> firstMap = ayatList.get(holder);
            String ayat_name = firstMap.get("parah_name");
            Log.d("tag", "Value of  name : " + ayat_name);
            toolbar.setTitle(ayat_name);
            setSupportActionBar(toolbar);
             adapter = new SimpleAdapter(ParahAyat.this, ayatList, R.layout.ayat_by_para, new String[]{"arabic", "translation_urdu"}, new int[]{R.id.parah, R.id.parah_urdu});
            listView.setAdapter((ListAdapter) adapter);

        }

        //Utility.setListViewHeightBasedOnChildren(listView);
        databaseAccess.close();
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        });
//Context menu code


                listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                        if(isActionMode){
                            return false;
                        }
                        ParahAyat.this.startActionMode(mActionModeCallback);
                        isActionMode = true;
                        view.setSelected(true);
                        Object clickItemObj = parent.getAdapter().getItem(position);
                        HashMap clickItemMap = (HashMap)clickItemObj;
                        arbi = (String)clickItemMap.get("arabic");
                        arbi_trans = (String)clickItemMap.get("translation_urdu");
                        return true;
                    }
                });







    }
    private void setupSharedPreferences() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        sharedPreferences.registerOnSharedPreferenceChangeListener(this);
    }
    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

        if (key.equals("urdu_text")) {
            DatabaseAccess databaseAccess = DatabaseAccess.getInstance(this);
            DatabaseAccess db = new DatabaseAccess(this);



            db.open();
            ayatList = db.getParah_urdu(holder);
            adapter = new SimpleAdapter(ParahAyat.this, ayatList, R.layout.ayat_by_para, new String[]{"arabic", "translation_urdu"}, new int[]{R.id.parah, R.id.parah_urdu});

            listView.setAdapter((ListAdapter) adapter);


        }
        if (key.equals("eng_text")) {
            DatabaseAccess databaseAccess = DatabaseAccess.getInstance(this);
            DatabaseAccess db = new DatabaseAccess(this);



            db.open();
            ayatList = db.getParah_eng(holder);
            adapter = new SimpleAdapter(ParahAyat.this, ayatList, R.layout.ayat_by_para, new String[]{"arabic", "translation_english"}, new int[]{R.id.parah, R.id.parah_urdu});
            listView.setAdapter((ListAdapter) adapter);
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        android.support.v7.preference.PreferenceManager.getDefaultSharedPreferences(this)
                .unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu, menu);

        //getting the search view from the menu
        MenuItem searchViewItem = menu.findItem(R.id.menuSearch);

        //getting search manager from systemservice
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);

        //getting the search view
        final SearchView searchView = (SearchView) searchViewItem.getActionView();

        //you can put a hint for the search input field
        //searchView.setQueryHint(");
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        //by setting it true we are making it iconified
        //so the search input will show up after taping the search iconified
        //if you want to make it visible all the time make it false
        searchView.setIconifiedByDefault(true);

        //here we will get the search query
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                //do the search here
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {


            case R.id.menuSettings:
                Intent intent = new Intent(getApplicationContext(), SettingsActivity.class);
                startActivity(intent);
                break;


        }
        return true;
    }
    private ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.context_horizontal_menu, menu);
            return true;
        }
        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }
        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.Itembookmarked:
                    Toast.makeText(ParahAyat.this,"Fav clicked  press", Toast.LENGTH_LONG).show();
                    mode.finish();
                    return true;
                case R.id.Itemplay:
                    View menuItemView = findViewById(R.id.Itemplay); // SAME ID AS MENU ID
                    PopupMenu popup = new PopupMenu(ParahAyat.this,  menuItemView, Gravity.CENTER);
                    popup.inflate(R.menu.menu_audio);
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {

                            switch (item.getItemId()) {
                                case R.id.play_arabic:
                                    String option = (String) item.getTitle();

                                    Log.d("tag","onclick play " + option);
                                    DatabaseAccess databaseAccess = DatabaseAccess.getInstance(getApplicationContext());
                                    DatabaseAccess db = new DatabaseAccess(getApplicationContext());
                                    db.open();
                                    ayatAudio = db.getAyatp_Audio(holder);
                                    HashMap<String, Integer> details =  ayatAudio.get(0);
                                    int ayat_ini = details.get("verse_initial");
                                    int ayat_end = details.get("verse_end");
                                    Intent intent = new Intent(getApplicationContext(), AudioService.class);
                                    intent.putExtra("ayat_s", ayat_ini);
                                    intent.putExtra("ayat_e", ayat_end);
                                    intent.putExtra("ayat_op", option);
                                    startService(intent);


                                    return true;

                                case R.id.play_arabicT:
                                    option = (String) item.getTitle();
                                    Log.d("tag","onclick play");
                                    db = new DatabaseAccess(getApplicationContext());
                                    db.open();
                                    ayatAudio = db.getAyatp_Audio(holder);
                                    details =  ayatAudio.get(0);
                                    ayat_ini = details.get("verse_initial");
                                    ayat_end = details.get("verse_end");
                                    intent = new Intent(getApplicationContext(), AudioService.class);
                                    intent.putExtra("ayat_s", ayat_ini);
                                    intent.putExtra("ayat_e", ayat_end);
                                    intent.putExtra("ayat_op", option);
                                    startService(intent);


                                    return true;

                                case R.id.play_trans:
                                    option = (String) item.getTitle();
                                    Log.d("tag","onclick play");
                                    db = new DatabaseAccess(getApplicationContext());
                                    db.open();
                                    ayatAudio = db.getAyatp_Audio(holder);
                                    details =  ayatAudio.get(0);
                                    ayat_ini = details.get("verse_initial");
                                    ayat_end = details.get("verse_end");
                                    intent = new Intent(getApplicationContext(), AudioService.class);
                                    intent.putExtra("ayat_s", ayat_ini);
                                    intent.putExtra("ayat_e", ayat_end);
                                    intent.putExtra("ayat_op", option);
                                    startService(intent);

                                    return true;

                                default:
                                    return true;

                            }
                        }
                    });


                    popup.show();
                    return true;
                case R.id.Itemshare:
                    Toast.makeText(ParahAyat.this,"share  clicked", Toast.LENGTH_LONG).show();
                    String finaal = arbi + arbi_trans;
                    Intent sharingIntent = new Intent(
                            android.content.Intent.ACTION_SEND);
                    sharingIntent.setType("text/plain");
                    //String shareBody = "Here is the share content body";
                    //sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,
                    //"Subject Here");
                    sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, finaal);
                    startActivity(Intent.createChooser(sharingIntent, "Share via"));
                    mode.finish();
                    return true;
                default:
                    return false;
            }
        }
        @Override
        public void onDestroyActionMode(ActionMode mode) {
            isActionMode = false;
        }
    };

}