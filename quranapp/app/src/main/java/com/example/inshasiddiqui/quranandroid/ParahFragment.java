package com.example.inshasiddiqui.quranandroid;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.SimpleCursorAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class ParahFragment extends Fragment {
    private ListView listView;


    public ParahFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //listView = (ListView) findViewsById(R.id.listView_p);
        View view = inflater.inflate(R.layout.details, container,
                false);
        listView = (ListView) findViewsById(view);



        DatabaseAccess databaseAccess = DatabaseAccess.getInstance(getActivity());

        DatabaseAccess db = new DatabaseAccess(getActivity());
        db.open();
        ArrayList<HashMap<String,String>> parahList = db.getParah();

       ListAdapter adapter = new SimpleAdapter(getActivity(), parahList, R.layout.fragment_parah,new String[]{"parah_name_urdu","parah_name_eng","tot_verses","parah_id"}, new int[]{R.id.parah_urdu, R.id.parah_name, R.id.verses,R.id.parah_id});
        listView.setAdapter((ListAdapter) adapter);
        databaseAccess.close();
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // TODO Auto-generated method stub

                // Getting listview click value into String variable.
                Object clickItemObj = parent.getAdapter().getItem(position);
                HashMap clickItemMap = (HashMap)clickItemObj;
                String itemid = (String)clickItemMap.get("parah_id");
                //String itemname = (String)clickItemMap.get("surat_name_urdu");
                Log.d("tag", "Value ofitemid : " + itemid );
                //Log.d("tag", "Value of itemname: " + itemname);


                //SM.sendData(itemid );
                Intent intent = new Intent(getActivity(), ParahAyat.class);

                //int rowid = cursor.getInt(0);
                //String name = cursor.getString(1);
                //String amount = cursor.getString(2);
                //String date = cursor.getString(3);
                intent.putExtra("aid", itemid);
                startActivity(intent);



            }
        });
        return view;

    }

    private View findViewsById(View view) {
        listView = (ListView) view.findViewById(R.id.listView_p);
        return listView;
    }

}
