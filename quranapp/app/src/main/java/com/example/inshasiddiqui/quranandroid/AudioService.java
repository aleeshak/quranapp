package com.example.inshasiddiqui.quranandroid;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import java.util.Objects;

import static java.lang.Integer.getInteger;

public class AudioService extends Service  {
    private MediaPlayer player;
    public int j = 0;
    public int limit_sp = 0;
    private String myUrl;
    //current position
    private int songPosn;
    // private final IBinder audioBind = new AudioBinder();
    public int i;
    public int limit;
    public String play_option;


    private static final String TAG = "MyService";
    String url;
    public MediaPlayer mp = new MediaPlayer();

    @Override
    public void onCreate() {
        Toast.makeText(this, "My Service Created", Toast.LENGTH_LONG).show();
        Log.d(TAG, "onCreate");

        // MediaPlayer mp = new MediaPlayer();
        mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
    }

    @Override
    public void onDestroy() {
        Toast.makeText(this, "My Service Stopped", Toast.LENGTH_LONG).show();
        Log.d(TAG, "onDestroy");

        mp.stop();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startid) {

        Log.d(TAG, "onStart");

         i = intent.getIntExtra("ayat_s",0);
       limit = intent.getIntExtra("ayat_e",0);
        play_option = intent.getStringExtra("ayat_op");
        Log.d("tag","audio sho " + i);
        Log.d("tag","audio e " + limit);
        switch (play_option){
            case "Only Arabic":
                int k;
                k = play_arabi();
            case "Arabic with translation":
                j =i;
                limit_sp = limit;
                k = play_arabi_with_trans();

                Log.d("tag", "below " + j + limit_sp);
                //mp.reset();
               // k = play_ar_tran(j);
            case "Only translation":
                k = play_trans_u(); //to do only translation me do checks dalney hai for language add play tran eng
        }



        /*Uri.Builder builder = new Uri.Builder();
        builder.scheme("https")
                .authority("cdn.alquran.cloud")
                .appendPath("media")
                .appendPath("audio")
                .appendPath("ayah")
                .appendPath("ar.alafasy")
                .appendPath(String.valueOf(i));
        String myUrl = builder.build().toString();


        try {
            mp.setDataSource(myUrl);
            mp.prepare();
            mp.start();
            setNextMediaForMediaPlayer(mp);
        } catch (Exception e) {
        }

        i = i;
        Log.d("tag", "value of i now " + i);*/
        return START_STICKY;

    }
    public int play_arabi(){
        Uri.Builder builder = new Uri.Builder();
        builder.scheme("https")
                .authority("cdn.alquran.cloud")
                .appendPath("media")
                .appendPath("audio")
                .appendPath("ayah")
                .appendPath("ar.alafasy")
                .appendPath(String.valueOf(i));
        String myUrl = builder.build().toString();


        try {
            mp.setDataSource(myUrl);
            mp.prepare();
            mp.start();
            setNextMediaForMediaPlayer(mp);
        } catch (Exception e) {
        }

        i = i;
        Log.d("tag", "value of i now " + i);
        return START_STICKY;

    }
    public int play_arabi_with_trans(){
        Uri.Builder builder = new Uri.Builder();
        builder.scheme("https")
                .authority("cdn.alquran.cloud")
                .appendPath("media")
                .appendPath("audio")
                .appendPath("ayah")
                .appendPath("ar.alafasy")
                .appendPath(String.valueOf(i));
        String myUrl = builder.build().toString();


        try {
            mp.setDataSource(myUrl);
            mp.prepare();
            mp.start();
            setNextMediaForMediaPlayer_sp(mp);
        } catch (Exception e) {
        }

        i = i;
        Log.d("tag", "value of i now " + i);
        return START_STICKY;

    }
    public int play_urdu(int j){
        int t =j;


        Uri.Builder builder = new Uri.Builder();
        builder.scheme("https")
                .authority("cdn.alquran.cloud")
                .appendPath("media")
                .appendPath("audio")
                .appendPath("ayah")
                .appendPath("ur.khan")
                .appendPath(String.valueOf(t));
        String myUrl = builder.build().toString();


        try {
            mp.setDataSource(myUrl);
            mp.prepare();
            mp.start();
            setNextMediaForMediaPlayer_ur_sp(mp);
        } catch (Exception e) {
        }

        i = i;
        Log.d("tag", "value of i now " + i);
        return START_STICKY;

    }
    public int play_eng(int j){
        int t =j;


        Uri.Builder builder = new Uri.Builder();
        builder.scheme("https")
                .authority("cdn.alquran.cloud")
                .appendPath("media")
                .appendPath("audio")
                .appendPath("ayah")
                .appendPath("en.walk")
                .appendPath(String.valueOf(t));
        String myUrl = builder.build().toString();


        try {
            mp.setDataSource(myUrl);
            mp.prepare();
            mp.start();
            setNextMediaForMediaPlayer_eng(mp);
        } catch (Exception e) {
        }

        i = i;
        Log.d("tag", "value of i now " + i);
        return START_STICKY;

    }
    public int play_trans_u (){
        Uri.Builder builder = new Uri.Builder();
        builder.scheme("https")
                .authority("cdn.alquran.cloud")
                .appendPath("media")
                .appendPath("audio")
                .appendPath("ayah")
                .appendPath("ur.khan")
                .appendPath(String.valueOf(i));
        String myUrl = builder.build().toString();


        try {
            mp.setDataSource(myUrl);
            mp.prepare();
            mp.start();
            setNextMediaForMediaPlayer_ur(mp);
        } catch (Exception e) {
        }

        i = i;
        Log.d("tag", "value of i now " + i);
        return START_STICKY;

    }
    public int play_trans_e (){
        Uri.Builder builder = new Uri.Builder();
        builder.scheme("https")
                .authority("cdn.alquran.cloud")
                .appendPath("media")
                .appendPath("audio")
                .appendPath("ayah")
                .appendPath("en.walk")
                .appendPath(String.valueOf(i));
        String myUrl = builder.build().toString();


        try {
            mp.setDataSource(myUrl);
            mp.prepare();
            mp.start();
            setNextMediaForMediaPlayer_eng(mp);
        } catch (Exception e) {
        }

        i = i;
        Log.d("tag", "value of i now " + i);
        return START_STICKY;

    }
    public void setNextMediaForMediaPlayer(MediaPlayer player) {
        player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                mp.reset();
                i++;

                if(i <= limit)
                {Uri.Builder builder = new Uri.Builder();
                    builder.scheme("https")
                            .authority("cdn.alquran.cloud")
                            .appendPath("media")
                            .appendPath("audio")
                            .appendPath("ayah")
                            .appendPath("ar.alafasy")
                            .appendPath(String.valueOf(i));
                    String myUrl = builder.build().toString();


                    try {
                        mp.setDataSource(myUrl);
                        mp.prepare();
                        mp.start();
                    } catch (Exception e) {
                    }


                }


            }
        });
    }
    public void setNextMediaForMediaPlayer_sp(MediaPlayer player) {
        player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                mp.reset();
                i++;

                if(i <= limit)
                {Uri.Builder builder = new Uri.Builder();
                    builder.scheme("https")
                            .authority("cdn.alquran.cloud")
                            .appendPath("media")
                            .appendPath("audio")
                            .appendPath("ayah")
                            .appendPath("ar.alafasy")
                            .appendPath(String.valueOf(i));
                    String myUrl = builder.build().toString();


                    try {
                        mp.setDataSource(myUrl);
                        mp.prepare();
                        mp.start();
                    } catch (Exception e) {
                    }


                } //tp do language check add
                play_urdu(j);

            }
        });
    }
    public void setNextMediaForMediaPlayer_ur(MediaPlayer player) {
        player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                mp.reset();
                i++;

                if(i <= limit)
                {Uri.Builder builder = new Uri.Builder();
                    builder.scheme("https")
                            .authority("cdn.alquran.cloud")
                            .appendPath("media")
                            .appendPath("audio")
                            .appendPath("ayah")
                            .appendPath("ur.khan")
                            .appendPath(String.valueOf(i));
                    String myUrl = builder.build().toString();


                    try {
                        mp.setDataSource(myUrl);
                        mp.prepare();
                        mp.start();
                    } catch (Exception e) {
                    }


                }

            }
        });
    }
    public void setNextMediaForMediaPlayer_ur_sp(MediaPlayer player) {
        player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                mp.reset();
                j++;

                if(j <= limit_sp)
                {Uri.Builder builder = new Uri.Builder();
                    builder.scheme("https")
                            .authority("cdn.alquran.cloud")
                            .appendPath("media")
                            .appendPath("audio")
                            .appendPath("ayah")
                            .appendPath("ur.khan")
                            .appendPath(String.valueOf(j));
                    String myUrl = builder.build().toString();


                    try {
                        mp.setDataSource(myUrl);
                        mp.prepare();
                        mp.start();
                    } catch (Exception e) {
                    }


                }

            }
        });
    }
    public void setNextMediaForMediaPlayer_eng(MediaPlayer player) {
        player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                mp.reset();
                i++;

                if(i <= limit)
                {Uri.Builder builder = new Uri.Builder();
                    builder.scheme("https")
                            .authority("cdn.alquran.cloud")
                            .appendPath("media")
                            .appendPath("audio")
                            .appendPath("ayah")
                            .appendPath("en.walk")
                            .appendPath(String.valueOf(j));
                    String myUrl = builder.build().toString();


                    try {
                        mp.setDataSource(myUrl);
                        mp.prepare();
                        mp.start();
                    } catch (Exception e) {
                    }


                }

            }
        });
    }
    public void setNextMediaForMediaPlayer_eng_sp(MediaPlayer player) {
        player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                mp.reset();
                j++;

                if(j <= limit_sp)
                {Uri.Builder builder = new Uri.Builder();
                    builder.scheme("https")
                            .authority("cdn.alquran.cloud")
                            .appendPath("media")
                            .appendPath("audio")
                            .appendPath("ayah")
                            .appendPath("en.walk")
                            .appendPath(String.valueOf(j));
                    String myUrl = builder.build().toString();


                    try {
                        mp.setDataSource(myUrl);
                        mp.prepare();
                        mp.start();
                    } catch (Exception e) {
                    }


                }

            }
        });
    }



}
