package com.example.inshasiddiqui.quranandroid;


import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class SurahFragment extends Fragment {
    private ListView listView;
    //SendMessage SM;

    public SurahFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //listView = (ListView) findViewsById(R.id.listView_p);

        View view = inflater.inflate(R.layout.details, container,
                false);
        listView = (ListView) findViewsById(view);



        DatabaseAccess databaseAccess = DatabaseAccess.getInstance(getActivity());

        DatabaseAccess db = new DatabaseAccess(getActivity());
        db.open();
        final ArrayList<HashMap<String, String>> surahhList = db.getSurah();
        HashMap<String, String> firstMap = surahhList.get(0);
        String someEntry = firstMap.get("surat_id");
        Log.d("tag", "Value of surah of someentry: " + someEntry);
        Log.d("tag", "Value of surah of firstmap: " + firstMap);
        Log.d("tag", "Value of surah of surahhlst: " + surahhList);




        ListAdapter adapter = new SimpleAdapter(getActivity(), surahhList, R.layout.fragment_surah,new String[]{"surat_name_urdu","surat_name_english","surat_id"}, new int[]{R.id.surah_urdu, R.id.surah_name, R.id.surah_id});
        listView.setAdapter((ListAdapter) adapter);
        databaseAccess.close();


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // TODO Auto-generated method stub

                // Getting listview click value into String variable.
                Object clickItemObj = parent.getAdapter().getItem(position);
                HashMap clickItemMap = (HashMap)clickItemObj;
                String itemid = (String)clickItemMap.get("surat_id");
                String itemname = (String)clickItemMap.get("surat_name_urdu");
                Log.d("tag", "Value ofitemid : " + itemid );
                Log.d("tag", "Value of itemname: " + itemname);


                //SM.sendData(itemid );
                Intent intent = new Intent(getActivity(), SurahAyat.class);

                //int rowid = cursor.getInt(0);
                //String name = cursor.getString(1);
                //String amount = cursor.getString(2);
                //String date = cursor.getString(3);
                intent.putExtra("aid", itemid);
                startActivity(intent);



            }
        });
        return view;

    }

    public View findViewsById(View view) {
        listView = (ListView) view.findViewById(R.id.listView_p);
        return listView;
    }
   /* interface SendMessage {
        void sendData(String message);
    }*/

    /*@Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            SM = (SendMessage) getActivity();
        } catch (ClassCastException e) {
            throw new ClassCastException("Error in retrieving data. Please try again");
        }
    }*/


}
