package com.example.inshasiddiqui.quranandroid;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;

public class SurahAyat extends AppCompatActivity {
    private ListView listView;

    // getting attached intent data


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_parah);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);

        int holder = Integer.parseInt(getIntent().getStringExtra("aid"));
        Log.d("tag", "Value of   : " + holder);


        //setContentView(R.layout.detail_parah);
        ListView listView = (ListView) findViewById(R.id.listView_p);
        DatabaseAccess databaseAccess = DatabaseAccess.getInstance(this);
        DatabaseAccess db = new DatabaseAccess(this);
        db.open();

            final ArrayList<HashMap<String, String>> ayatList = db.getSurat_urdu(holder);
            if(holder < ayatList.size()) {
                HashMap<String, String> firstMap = ayatList.get(holder);
                String ayat_name = firstMap.get("surat_name_english");
                Log.d("tag", "Value of  name : " + ayat_name);
                toolbar.setTitle(ayat_name);
                setSupportActionBar(toolbar);
                ListAdapter adapter = new SimpleAdapter(SurahAyat.this, ayatList, R.layout.ayat_by_surah, new String[]{"arabic", "translation_urdu"}, new int[]{R.id.surah, R.id.surah_urdu});
                listView.setAdapter((ListAdapter) adapter);
            }
            //Utility.setListViewHeightBasedOnChildren(listView);
            databaseAccess.close();
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);
                }
            });



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu, menu);

        //getting the search view from the menu
        MenuItem searchViewItem = menu.findItem(R.id.menuSearch);

        //getting search manager from systemservice
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);

        //getting the search view
        final SearchView searchView = (SearchView) searchViewItem.getActionView();

        //you can put a hint for the search input field
        //searchView.setQueryHint(");
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        //by setting it true we are making it iconified
        //so the search input will show up after taping the search iconified
        //if you want to make it visible all the time make it false
        searchView.setIconifiedByDefault(true);

        //here we will get the search query
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                //do the search here
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {


            case R.id.menuSettings:
                Intent intent = new Intent(getApplicationContext(), TranslationActivity.class);
                startActivity(intent);
                break;


        }
        return true;
    }


}

