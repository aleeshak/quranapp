package com.example.inshasiddiqui.quranandroid;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;

public class DatabaseAccess {
    private SQLiteOpenHelper openHelper;
    private SQLiteDatabase database;
    private SQLiteDatabase db1;

    private static DatabaseAccess instance;
    private static final String KEY_ID = "parah_id";
    private static final String KEY_PARAH_URDU = "parah_name_urdu";
    private static final String KEY_PARAH_ENG = "parah_name_eng";
    private static final String KEY_TOT_VERSES = "tot_verses";
    private static final String KEY_SURAT_ID = "surat_id";
    private static final String KEY_SURAT_URDU = "surat_name_urdu";
    private static final String KEY_SURAT_ENG = "surat_name_english";
    private static final String KEY_SURAT_ARABI = "arabic";
    private static final String KEY_SURAT_URDU_TRANS = "translation_urdu";
    private static final String KEY_SURAT_ENG_TRANS = "surat_eng_trans";


    protected DatabaseAccess(Context context) {
        this.openHelper = new DatabaseOpenHelper(context);
    }


    public static DatabaseAccess getInstance(Context context) {
        if (instance == null) {
            instance = new DatabaseAccess(context);
        }
        return instance;
    }


    public void open() {
        this.database = openHelper.getReadableDatabase();
    }


    public void close() {
        if (database != null) {
            this.database.close();
        }
    }


    public ArrayList<HashMap<String, String>> getParah() {
        ArrayList<HashMap<String, String>>  list = new ArrayList<>();
        db1=openHelper.getReadableDatabase();
        Cursor cursor = database.rawQuery("SELECT * FROM tbl_Parah ORDER BY parah_id", null);
        //cursor.moveToFirst();
        while (cursor.moveToNext()){
            HashMap<String,String> parah = new HashMap<>();
            parah.put("parah_name_urdu",cursor.getString(cursor.getColumnIndex(KEY_PARAH_URDU)));
            parah.put("parah_name_eng",cursor.getString(cursor.getColumnIndex(KEY_PARAH_ENG)));
            parah.put("tot_verses",cursor.getString(cursor.getColumnIndex(KEY_TOT_VERSES)));
            parah.put("parah_id",cursor.getString(cursor.getColumnIndex(KEY_ID)));
            list.add(parah);
        }

        cursor.close();
        return list;
    }
    public ArrayList<HashMap<String, String>> getSurah() {
        ArrayList<HashMap<String, String>>  list = new ArrayList<>();
        db1=openHelper.getReadableDatabase();
        Cursor cursor = database.rawQuery("SELECT DISTINCT surat_id,surat_name_urdu,surat_name_english FROM tbl_QuranComplete ", null);
        //cursor.moveToFirst();
        while (cursor.moveToNext()){
            HashMap<String,String> surah = new HashMap<>();
            surah.put("surat_name_urdu",cursor.getString(cursor.getColumnIndex(KEY_SURAT_URDU)));
            surah.put("surat_name_english",cursor.getString(cursor.getColumnIndex(KEY_SURAT_ENG)));
            surah.put("surat_id",cursor.getString(cursor.getColumnIndex(KEY_SURAT_ID)));
            list.add(surah);
        }

        cursor.close();
        return list;
    }
    public ArrayList<HashMap<String, String>> getSurat_urdu(int suratid) {
        ArrayList<HashMap<String, String>>  list = new ArrayList<>();
        int s_id =  suratid;
        Log.d("tag","valueeee "+ s_id);
        db1=openHelper.getReadableDatabase();
        Cursor cursor = database.rawQuery("SELECT DISTINCT  surat_id,surat_name_english, arabic ,translation_urdu FROM tbl_QuranComplete  WHERE surat_id = "+s_id+" ", null);
        //cursor.moveToFirst();
        while (cursor.moveToNext()){
            HashMap<String,String> surah = new HashMap<>();
            surah.put("arabic",cursor.getString(cursor.getColumnIndex(KEY_SURAT_ARABI)));
            surah.put("translation_urdu",cursor.getString(cursor.getColumnIndex(KEY_SURAT_URDU_TRANS)));
            surah.put("surat_name_english",cursor.getString(cursor.getColumnIndex(KEY_SURAT_ENG)));
            //surah.put("surat_id",cursor.getString(cursor.getColumnIndex(KEY_SURAT_ID)));
            list.add(surah);
        }

        cursor.close();
        return list;
    }
}
