package com.example.inshasiddiqui.quranandroid;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.SimpleCursorAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class ParahFragment extends Fragment {
    private ListView listView;


    public ParahFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //listView = (ListView) findViewsById(R.id.listView_p);
        View view = inflater.inflate(R.layout.details, container,
                false);
        listView = (ListView) findViewsById(view);



        DatabaseAccess databaseAccess = DatabaseAccess.getInstance(getActivity());

        DatabaseAccess db = new DatabaseAccess(getActivity());
        db.open();
        ArrayList<HashMap<String,String>> parahList = db.getParah();

       ListAdapter adapter = new SimpleAdapter(getActivity(), parahList, R.layout.fragment_parah,new String[]{"parah_name_urdu","parah_name_eng","tot_verses","parah_id"}, new int[]{R.id.parah_urdu, R.id.parah_name, R.id.verses,R.id.parah_id});
        listView.setAdapter((ListAdapter) adapter);
        databaseAccess.close();
        return view;

    }

    private View findViewsById(View view) {
        listView = (ListView) view.findViewById(R.id.listView_p);
        return listView;
    }

}
